# -*- coding: utf-8 -*-

{
    'name': "Custom Barcode",
    'summary': "Custom barcode action",
    'description': """
Custom barcode action for engineer challenge (Hendra)
    """,
    'category': 'Operations/Inventory',
    'version': '13.0.1',
    'depends': ['stock_barcode'],
    'data': [
        'views/assets.xml',
        'views/action_views.xml',
        'views/res_config_settings_views.xml',
    ],
    'installable': True,
    'application': False,
}
