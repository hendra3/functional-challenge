odoo.define('custom_barcode.ClientAction', ['stock_barcode.ClientAction'], function(require) {
'use strict';

var ClientAction = require('stock_barcode.ClientAction');

ClientAction.include({
    init: function(parent, action) {
        this._super.apply(this, arguments);
        this.commands['O-CMD.READY-IN'] = this._onReadyIn.bind(this);
        this.commands['O-CMD.READY-OUT'] = this._onReadyOut.bind(this);
    },
    _onReadyIn: function() {
        var self = this;
        self._save().then(function() {
            self.do_action('custom_barcode.action_ready_in', {
                clear_breadcrumbs: true,
            });
        });
    },
    _onReadyOut: function() {
        var self = this;
        self._save().then(function() {
            self.do_action('custom_barcode.action_ready_out', {
                clear_breadcrumbs: true,
            });
        });
    },
});

});